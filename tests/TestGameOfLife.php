<?php

require_once '../src/GameOfLife.php';

class TestGameOfLife extends PHPUnit_Framework_TestCase
{
	function testThereIsLife()
	{
		$game = new GameOfLife();

		$this->assertEquals(
'........
....*...
...**...
........', $game->getWorld() );
	}
}
